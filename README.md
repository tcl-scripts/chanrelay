# ChanRelay v5.0
[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/crazycat)

This TCL is a complete relay script wich works with botnet.

## DESCRIPTION

This TCL is a complete relay script wich works with botnet.
All you have to do is to include this tcl in all the eggdrop who are concerned by it.

You can use it as a spy or a full duplex communication tool.

It don't mind if the eggdrops are on the same server or not, it just mind about the channels and the handle of each eggdrop.

## OPERATOR COMMANDS
Removed in version 5

## TRANSLATIONS
Actually, english and french are totally supported. Spanish, german and portuguese are google translate, but you can correct the files in the [gitlab repository](https://gitlab.com/tcl-scripts/chanrelay/-/tree/master/crtools).

Languages files consist in 2 lists: `"source in english"` `"translated sentence"`

**Do not change the source datas, in language file nor in script.**

## CHANGELOG
see [CHANGELOG](CHANGELOG)

## TODO
- [ ] Enhance configuration
- [x] Allow save of configuration
- [x] Multi-languages
- [x] uniformisation of oper commands
- [x] make oper commands working
