# chanrelay.tcl 4.0.2
#
# A way to link your channels
#
# Author: CrazyCat <crazycat@c-p-f.org>
# https://www.eggdrop.fr
# irc.zeolia.net #eggdrop
#
# Declare issues at https://gitlab.com/tcl-scripts/chanrelay
# No issue, no bug :)
#
## DESCRIPTION ##
#
# This TCL is a complete relay script wich works with botnet.
# All you have to do is to include this tcl in all the eggdrop who
# are concerned by it.
#
# You can use it as a spy or a full duplex communication tool.
#
# It don't mind if the eggdrops are on the same server or not,
# it just mind about the channels and the handle of each eggdrop.

## CONFIGURATION ##
#
# For each eggdrop in the relay, you have to
# indicate his botnet nick, the chan and the network.
#
# Syntax:
# set regg(USERNAME) {
#   "chan"      "#CHANNEL"
#   "network"   "NETWORK"
#}
# with:
# USERNAME : The username sets in eggdrop.conf (case-sensitive)
# optionaly, you can override default values:
# * highlight (0/1/2/3): is speaker highlighted ? (no/bold/undelined/gray)
# * snet (y/n): is speaker'network shown ?
# * transmit (y/n): does eggdrop transmit his channel activity ?
# * receive (y/n): does eggdrop diffuse other channels activity ?
# * syn_topic (y/n): if set to Yes, the eggdrop will
#   synchronize the channel topic when changed on
#   another chan of the relay
# * col_act (color) : color used to display actions
# * col_mode (color) : color used to display mode changement
# * col_jqp (color) : color used to display join/quit/part


namespace eval crelay {

	#####################
	### CONFIGURATION ###
	#####################

	###     BASIC     ###
	variable regg
	set regg(excalibur) {
      "display"   "Excalibur"
		"chan"      "#eggdrop"
		"network"   "Zeolia"
		"highlight" 0
		"log"       "y"
		"syn_topic" "y"
		"col_act"   "lightred"
		"col_jpq"   "lightblue"
		"col_mode"  "green"
		"usermask"	"<%nick%@%network%>"
	}

	set regg(crazyegg) {
      "display"   "CrazyEgg"
		"chan"      "#eggdrop"
		"network"   "Epiknet"
		"highlight" 3
	}

	set regg(cc_egg) {
      "display"   "CC_Egg"
		"chan"      "#eggdrop-fr"
		"network"   "Libera"
		"syn_topic"	"y"
	}

	# /!\ You can edit following values but 
	# DO NOT remove any
	variable default
	set default {
      "display"   ${nick}
		"highlight" 1
		"snet"      "y"
		"transmit"  "y"
		"receive"   "y"
		"log"       "n"
		"syn_topic" "n"
		"col_act"   "purple"
		"col_jpq"   "cyan"
		"col_mode"  "green"
		"usermask"	"(%nick%@%network%)"
	}
	
	###     GLOBAL     ###
	# debug mode : set to 1 to enable
	set debug 1
	
	# language to use : default is english, french, spanish, german and portuguese available
	set lang french
	
	# Fill this list with the nick of the users
	# who WON'T BE relayed, as services bot
	variable users_excluded {\[Guru\] Pan}

	# Fill this list with the nick of the users
	# wich will be THE ONLY ONES to be relayed
	variable users_only {}

	# max length of a message
	variable msglen 350
	
	###     FILES     ###
	# Path and name of the config file
	# %b will be replaced with the botnick
	variable crconf "%b.chanrelay.db"
	
	# Path and name of the logfile (used for debug)
	variable crlog "chanrelay.log"
	
	###     INITIAL     ###
	# transmission configuration
	set trans_pub "y"; # transmit the pub
	set trans_act "y"; # transmit the actions (/me)
	set trans_nick "y"; # transmit the nick changement
	set trans_join "y"; # transmit the join
	set trans_part "y"; # transmit the part
	set trans_quit "y"; # transmit the quit
	set trans_topic "y"; # transmit the topic changements
	set trans_kick "y"; # transmit the kicks
	set trans_mode "y"; #transmit the mode changements
	set trans_who "y"; # transmit the who list

	# reception configuration
	set recv_pub "y"; # recept the pub
	set recv_act "y"; # recept the actions (/me)
	set recv_nick "y"; # recept the nick changement
	set recv_join "y"; # recept the join
	set recv_part "y"; # recept the part
	set recv_quit "y"; # recept the quit
	set recv_topic "y"; # recept the topic changements
	set recv_kick "y"; # recept the kicks
	set recv_mode "y"; # recept the mode changements
	set recv_who "y"; # recept the who list
}

####################################
#    DO NOT EDIT ANYTHING BELOW    #
####################################
namespace eval crelay {

   package require hook
   
	variable author "CrazyCat"
	variable version "4.0.2"
	variable userlist
	variable hlnick
	variable snet
	variable syn_topic
	set topicsynced 0
	
	variable crpath "[file dirname [file normalize [info script]]]/crtools/"
	variable fname 
	
	proc init {args} {
		variable me
		array set me $::crelay::default
		array set me $::crelay::regg($::username)
		regsub -all %b ${::crelay::crpath}${::crelay::crconf} $::username fname
		if { [file exists $fname] } {
			::crelay::preload
		}
		if { $me(transmit) == "y" } {
			bind msg o|o "trans" ::crelay::set:trans
			if { $::crelay::trans_pub == "y" } { bind pubm - * ::crelay::trans:pub }
			if { $::crelay::trans_act == "y" } { bind ctcp - "ACTION" ::crelay::trans:act }
			if { $::crelay::trans_nick == "y" } { bind nick - * ::crelay::trans:nick }
			if { $::crelay::trans_join == "y" } { bind join - * ::crelay::trans:join }
			if { $::crelay::trans_part == "y" } { bind part - * ::crelay::trans:part }
			if { $::crelay::trans_quit == "y" } {
				bind sign - * ::crelay::trans:quit
				bind evnt - disconnect-server ::crelay::trans:selfquit
			}
			if { $::crelay::trans_topic == "y" } { bind topc - * ::crelay::trans:topic }
			if { $::crelay::trans_kick == "y" } { bind kick - * ::crelay::trans:kick }
			if { $::crelay::trans_mode == "y" } { bind raw - "MODE" ::crelay::trans:mode }
			if { $::crelay::trans_who == "y" } { bind pub - "!who" ::crelay::trans:who }
		}
		if { $me(receive) =="y" } {
			bind msg o|o "recv" ::crelay::set:recv
			if { $::crelay::recv_pub == "y" } { bind bot - ">pub" ::crelay::recv:pub }
			if { $::crelay::recv_act == "y" } { bind bot - ">act" ::crelay::recv:act }
			if { $::crelay::recv_nick == "y" } { bind bot - ">nick" ::crelay::recv:nick }
			if { $::crelay::recv_join == "y" } { bind bot - ">join" ::crelay::recv:join }
			if { $::crelay::recv_part == "y" } { bind bot - ">part" ::crelay::recv:part }
			if { $::crelay::recv_quit == "y" } { bind bot - ">quit" ::crelay::recv:quit }
			if { $::crelay::recv_topic == "y" } { bind bot - ">topic" ::crelay::recv:topic }
			if { $::crelay::recv_kick == "y" } { bind bot - ">kick" ::crelay::recv:kick }
			if { $::crelay::recv_mode == "y" } { bind bot - ">mode" ::crelay::recv:mode }
			if { $::crelay::recv_who == "y" } {
				bind bot - ">who" ::crelay::recv:who
				bind bot - ">wholist" ::crelay::recv:wholist
			}
         bind bot - ">acktopic" ::crelay::recv:acktopic
			bind disc - * ::crelay::recv:disc
			bind link - * ::crelay::recv:link
			bind bot - ">list" ::crelay::recv:list
			bind bot - ">users" ::crelay::recv:users
		}

		::crelay::set:hl $me(highlight);

		if { $me(log) == "y"} {
			logfile sjpk $me(chan) "logs/[string range $me(chan) 1 end].log"
		}
		bind msg -|o "rc.status" ::crelay::help:status
		bind msg - "rc.help" ::crelay::help:cmds
		bind msg -|o "rc.light" ::crelay::set:light
		bind msg -|o "rc.net" ::crelay::set:snet
		bind msg -|o "rc.syntopic" ::crelay::set:syn_topic
		bind msg -|o "rc.debug" ::crelay::set:debug
		bind bot - ">notop" ::crelay::recv:error

		variable eggdrops
		variable chans
		variable networks
		foreach bot [array names ::crelay::regg] {
			array set tmp $::crelay::regg($bot)
			lappend eggdrops $bot
			lappend chans $tmp(chan)
			lappend networks $tmp(network)
		}
		::crelay::save
		bind evnt -|- prerehash ::crelay::deinit

		package forget ChanRelay
		package provide ChanRelay $::crelay::version
	}

	proc preload {args} {
		regsub -all %b ${::crelay::crpath}${::crelay::crconf} $::username fname
		if { [file exists $fname] } {
			set fp [open $fname r]
			set settings [read -nonewline $fp]
			close $fp
			foreach line [split $settings "\n"] {
				set lset [split $line "|"]
				switch [lindex $lset 0] {
					transmit { set ::crelay::me(transmit) [lindex $lset 1] }
					receive { set ::crelay::me(receive) [lindex $lset 1] }
					snet { set ::crelay::me(snet) [lindex $lset 1] }
					highlight { set ::crelay::me(highligt) [lindex $lset 1] }
					syn_topic { set ::crelay::me(syn_topic) [lindex $lset 1] }
					col_act { set ::crelay::me(col_act) [lindex $lset 1] }
					col_mode { set ::crelay::me(col_mode) [lindex $lset 1] }
					col_jpq { set ::crelay::me(col_jpq) [lindex $lset 1] }
					default {
						set ::crelay::[lindex $lset 0] [lindex $lset 1]
					}
				}
			}
		} else {
			::crelay::save
		}
	}

	# Save all settings in a file
	proc save {args} {
		regsub -all %b ${::crelay::crpath}${::crelay::crconf} $::username fname
		set fp [open $fname w]
		puts $fp "transmit|$::crelay::me(transmit)"
		puts $fp "receive|$::crelay::me(receive)"
		puts $fp "snet|$::crelay::me(snet)"
		puts $fp "highlight|$::crelay::me(highlight)"
		puts $fp "col_act|$::crelay::me(col_act)"
		puts $fp "col_mode|$::crelay::me(col_mode)"
		puts $fp "col_jpq|$::crelay::me(col_jpq)"
		puts $fp "trans_pub|$::crelay::trans_pub"
		puts $fp "trans_act|$::crelay::trans_act"
		puts $fp "trans_nick|$::crelay::trans_nick"
		puts $fp "trans_join|$::crelay::trans_join"
		puts $fp "trans_part|$::crelay::trans_part"
		puts $fp "trans_quit|$::crelay::trans_quit"
		puts $fp "trans_topic|$::crelay::trans_topic"
		puts $fp "trans_kick|$::crelay::trans_kick"
		puts $fp "trans_mode|$::crelay::trans_mode"
		puts $fp "trans_who|$::crelay::trans_who"
		puts $fp "recv_pub|$::crelay::recv_pub"
		puts $fp "recv_act|$::crelay::recv_act"
		puts $fp "recv_nick|$::crelay::recv_nick"
		puts $fp "recv_join|$::crelay::recv_join"
		puts $fp "recv_part|$::crelay::recv_part"
		puts $fp "recv_quit|$::crelay::recv_quit"
		puts $fp "recv_topic|$::crelay::recv_topic"
		puts $fp "recv_kick|$::crelay::recv_kick"
		puts $fp "recv_mode|$::crelay::recv_mode"
		puts $fp "recv_who|$::crelay::recv_who"
		puts $fp "syn_topic|$::crelay::me(syn_topic)"
		close $fp
		::crelay::preload
	}

	proc deinit {args} {
		putlog "Starting unloading CHANRELAY $::crelay::version"
		::crelay::save
		regsub %b $::crelay::crconf $::username fname
		putlog "CHANRELAY: Settings are saved in $fname"
		foreach binding [lsearch -inline -all -regexp [binds *[set ns [::tcl::string::range ::crelay 2 end]]*] " \{?(::)?$ns"] {
			unbind [lindex $binding 0] [lindex $binding 1] [lindex $binding 2] [lindex $binding 4]
		}
		putlog "CHANRELAY $::crelay::version unloaded"
		package forget ChanRelay
		namespace delete ::crelay
	}

	variable hlnick
	variable snet
	variable syn_topic
	set topicsynced 0
	
	# Setting of hlnick
	proc set:light { nick uhost handle arg } {
		# message binding
		switch [string tolower $arg] {
			"bo" { ::crelay::set:hl 1; }
			"un" { ::crelay::set:hl 2; }
			"gr" { ::crelay::set:hl 3; }
			"off" { ::crelay::set:hl 0; }
			default { puthelp "NOTICE $nick :[::msgcat::mc "you must chose \002(bo)\002ld , \037(un)\037derline, \00314(gr)\003ay or (off)"]" }
		}
		::crelay::save
		return 0;
	}

	proc set:hl { arg } {
		# global hlnick setting function
		switch [string tolower $arg] {
			1 { set ::crelay::hlnick "\002"; }
			2 { set ::crelay::hlnick "\037"; }
			3 { set ::crelay::hlnick "\00314"; }
			default { set ::crelay::hlnick ""; }
		}
	}
	
	variable colors { "white" "\00300" "black" "\00301" "blue" "\00302" "green" "\00303"
		"lightred" "\00304" "brown" "\00305" "purple" "\00306" "orange" "\00307"
		"yellow" "\00308" "lightgreen" "\00309" "cyan" "\00310" "lightcyan" "\00311"
		"lightblue" "\00312" "pink" "\00313" "grey" "\00314" "lightgrey" "\00315" }

	proc colorize {type text} {
		set text [stripcodes abcgru $text]
		if {$type eq "act" && $::crelay::me(col_act) ne ""} {
			set text "[::tcl::string::map $::crelay::colors $::crelay::me(col_act)]$text\003"
		} elseif {$type eq "mode" && $::crelay::me(col_mode) ne ""} {
			set text "[::tcl::string::map $::crelay::colors $::crelay::me(col_mode)]$text\003"
		} elseif { $type eq "jpq" && $::crelay::me(col_jpq) ne ""} {
			set text "[::tcl::string::map $::crelay::colors $::crelay::me(col_jpq)]$text\003"
		}
		return $text
	}
	
	# Setting of show network
	proc set:snet {nick host handle arg } {
		set arg [string tolower $arg]
		if { $arg == "yes" } {
			set ::crelay::snet "y"
			puthelp "NOTICE $nick :[::msgcat::mc "Network is now showed"]"
		} elseif { $arg == "no" } {
			set ::crelay::snet "n"
			puthelp "NOTICE $nick :[::msgcat::mc "Network is now hidden"]"
		} else {
			puthelp "NOTICE $nick :[::msgcat::mc "You must chose yes or no"]"
			return 0
		}
		::crelay::save
	}

	proc set:syn_topic {nick host handle arg} {
		set arg [string tolower $arg]
		if { $arg == "yes" } {
			set ::crelay::syn_topic "y"
			puthelp "NOTICE $nick :[::msgcat::mc "Topic synchro is now enabled"]"
		} elseif { $arg == "no" } {
			set ::crelay::syn_topic "n"
			puthelp "NOTICE $nick :[::msgcat::mc "Topic synchro is now disabled"]"
		} else {
			puthelp "NOTICE $nick :[::msgcat::mc "You must chose yes or no"]"
			return 0
		}
	}
	
	proc set:debug { nick host handle arg} {
		set arg [string tolower $arg]
		if { $arg == "yes" } {
			set ::crelay::debug 1
			puthelp "NOTICE $nick :[::msgcat::mc "Debug mode is now enabled"]"
		} elseif { $arg == "no" } {
			set ::crelay::debug 0
			puthelp "NOTICE $nick :[::msgcat::mc "Debug mode is now disabled"]"
		} else {
			puthelp "NOTICE $nick :[::msgcat::mc "Debug mode is actually setted to %1\$s" $::crelay::debug]"
			return 0
		}
	}

	# proc setting of transmission by msg
	proc set:trans { nick host handle arg } {
		if { $::crelay::me(transmit) == "y" } {
			if { $arg == "" } {
				putquick "NOTICE $nick :[::msgcat::mc "You'd better try /msg %1\$s trans help" $::botnick]"
			}
			if { [lindex [split $arg] 0] == "help" } {
				putquick "NOTICE $nick :[::msgcat::mc "Usage is /msg %1\$s trans <setting> on|off" $::botnick]"
				putquick "NOTICE $nick :[::msgcat::mc "with <setting> in pub, act, nick, join, part, quit, topic, kick, mode, who"]"
				return 0
			} else {
				switch [lindex [split $arg] 0] {
					"pub" { set type pubm }
					"act" { set type ctcp }
					"nick" { set type nick }
					"join" { set type join }
					"part" { set type part }
					"quit" { set type sign }
					"topic" { set type topc }
					"kick" { set type kick }
					"mode" { set type mode }
					"who" { set type who }
					default {
						putquick "NOTICE $nick :[::msgcat::mc "Bad setting. Try /msg %1\$s trans help" $::botnick]"
						return 0
					}
				}
				set proc_change "::crelay::trans:[lindex [split $arg] 0]"
				set mod_change "::crelay::trans_[lindex [split $arg] 0]"
				if { [lindex [split $arg] 1] eq "on" } {
					if { $type eq "mode" } {
						bind raw - "MODE" ::crelay::trans:mode
					} else {
						bind $type - * $proc_change
					}
					if { $type eq "sign"} {
						bind evnt - disconnect-server ::crelay::trans:selfquit
					}
					set ${mod_change} "y"
					putserv "NOTICE $nick :[::msgcat::mc "Transmission of %1\$s enabled" [lindex [split $arg] 0]]"
				} elseif { [lindex [split $arg] 1] eq "off" } {
					if { $type eq "mode" } {
						unbind raw - "MODE" ::crelay::trans:mode
					} else {
						unbind $type - * $proc_change
					}
					if { $type eq "sign"} {
						unbind evnt - disconnect-server ::crelay::trans:selfquit
					}
					set ${mod_change} "n"
					putserv "NOTICE $nick :[::msgcat::mc "Transmission of %1\$s disabled" [lindex [split $arg] 0]]"
				} else {
					putquick "NOTICE $nick :[::msgcat::mc "%1\$s is not a correct value, choose \002on\002 or \002off\002" [lindex [split $arg] 1]]"
				}
			}
		} else {
			putquick "NOTICE $nick :[::msgcat::mc "Transmission is not activated, you can't change anything"]"
		}
		::crelay::save
	}

	# proc setting of reception by msg
	proc set:recv { nick host handle arg } {
		if { $::crelay::me(receive) == "y" } {
			if { $arg == "" } {
				putquick "NOTICE $nick :[::msgcat::mc "You'd better try /msg %1\$s recv help" $::botnick]"
			}
			if { [lindex [split $arg] 0] == "help" } {
				putquick "NOTICE $nick :[::msgcat::mc "Usage is /msg %1\$s recv <setting> on|off" $::botnick]"
				putquick "NOTICE $nick :[::msgcat::mc "with <setting> in pub, act, nick, join, part, quit, topic, kick, mode, who"]"
				return 0
			} else {
				switch [lindex [split $arg] 0] {
					"pub" -
					"act" -
					"nick" -
					"join" -
					"part" -
					"quit" -
					"topic" -
					"kick" -
					"mode" -
					"who" { set type [lindex [split $arg] 0] }
					default {
						putquick "NOTICE $nick :[::msgcat::mc "Bad setting. Try /msg %1\$s recv help" $::botnick]"
						return 0
					}
				}
				set change ">$type"
				set proc_change "::crelay::recv:$type"
				set mod_change "::crelay::recv_$type"
				if { [lindex [split $arg] 1] eq "on" } {
					bind bot - $change $proc_change
					set ${mod_change} "y"
					putserv "NOTICE $nick :[::msgcat::mc "Reception of %1\$s enabled" $type]"
				} elseif { [lindex [split $arg] 1] == "off" } {
					unbind bot - $change $proc_change
					set ${mod_change} "n"
					putserv "NOTICE $nick :[::msgcat::mc "Reception of %1\$s disabled" $type]"
				} else {
					putquick "NOTICE $nick :[::msgcat::mc "%1\$s is not a correct value, choose \002on\002 or \002off\002" [lindex [split $arg] 1]]"
				}
			}
		} else {
			putquick "NOTICE $nick :[::msgcat::mc "Reception is not activated, you can't change anything"]"
		}
		::crelay::save
	}
	
	# Generates an user@network name
	# based on nick and from bot using usermask setting
	proc make:user { nick frm_bot } {
		if {[string length $::crelay::hlnick] > 0 } {
			set ehlnick [string index $::crelay::hlnick 0]
		} else {
			set ehlnick ""
		}
		set umask $::crelay::me(usermask)
		array set him $::crelay::regg($frm_bot)
		regsub -all %network% $umask $him(network) umask
		if {$nick == "*"} {
			regsub -all {(%nick%|@)} $umask "" umask
			set speaker [concat "$::crelay::hlnick$umask$ehlnick"]
		} else {
			regsub -all %nick% $umask $nick umask
			set speaker $::crelay::hlnick$umask$ehlnick
		}
		return $speaker
	}
	
	# Logs virtual channel activity 
   # Needed : type, nick, uhost, chan, line
   # Remove: lev
	proc cr:log { type nick uhost chan line } {
		if { $::crelay::me(log) ne "y" } {
         return
      }
      if {$::crelay::debug == 1} {
         ::crelay::dlog "*** Logger $type : $nick $line"
      }
      switch $type {
         ACT {
            hook call logger action $nick $chan $line
            putloglev p $chan "<$nick> ACTION $line"
         }
         JOIN {
            hook call logger join $nick $chan
            putloglev j $chan "$nick ($uhost) joined $chan"
         }
         PART {
            hook call logger part $nick $chan $line
            putloglev j $chan "$nick ($uhost) left $chan ($line)"
         }
         QUIT {
            hook call logger quit $nick $chan $line
            putloglev j $chan "$nick ($uhost) left irc: Quit: $line"
         }
         NICK {
            hook call logger nick $nick $chan $line
            putloglev j $chan "Nick change: $nick -> $line"
         }
         TOPIC {
            hook call logger topic $nick $chan $line
            putloglev j $chan "Topic changed on $chan by $nick!$uhost: $line"
         }
         default {
            hook call logger pubm $nick $chan $line
            putloglev p $chan "<$nick> $line"
         }
      }
	}
	
	# Global transmit procedure
	proc trans:bot { usercmd chan usernick text } {
		if { $::crelay::debug == 1 } { dlog "Transmission $usercmd from $usernick / $chan" }
		if {[llength $::crelay::users_only]>0 && [lsearch -nocase $::crelay::users_only $usernick]==-1} {
			return 0
		}
		if {[llength $::crelay::users_excluded]>0 && [lsearch -nocase $::crelay::users_excluded $usernick]!=-1} {
			return 0
		}
		set transmsg [concat $usercmd $usernick $text]
		set ::crelay::eob 0
		if {[string tolower $chan] == [string tolower $::crelay::me(chan)]} {
			foreach bot [array names ::crelay::regg] {
				if {$bot != $::username && [islinked $bot]} {
					putbot $bot $transmsg
					if { $::crelay::debug == 1 } { dlog "Sent to $bot : $transmsg" }
					if {$usercmd == ">who" } { incr ::crelay::eob }
				}
			}
			
		} else {
			return 0
		}
	}

	# proc transmission of pub (trans_pub = y)
	proc trans:pub {nick uhost hand chan text} {
      if {[string index $text 0] eq "\001"} { return }
		if { [string tolower [lindex [split $text] 0]] eq "!who" } { return 0; }
      if {$::crelay::debug == 1} { ::crelay::dlog "*** Catched PUB : $nick $text" }
		foreach splmsg [::crelay::split_line $text [expr {$::crelay::msglen - [::tcl::string::length [::crelay::make:user $nick $::username]]}]] {
			if { $::crelay::debug == 1 } { dlog "Prepare transmission : >pub $chan $nick $splmsg" }
			::crelay::trans:bot ">pub" $chan $nick $splmsg
		}
	}
	
	# proc transmission of action (trans_act = y)
	proc trans:act {nick uhost hand chan key text} {
		set arg [concat $key $text]
      if {$::crelay::debug == 1} { ::crelay::dlog "*** Catched ACTION : $nick $text" }
		if { $::crelay::debug == 1 } { dlog "Prepare transmission : >act $chan $nick $arg" }
		::crelay::trans:bot ">act" $chan $nick $arg
	}
	
	# proc transmission of nick changement
	proc trans:nick {nick uhost hand chan newnick} {
		if { $::crelay::debug == 1 } { dlog "Prepare transmission : >nick $chan $nick $newnick" }
		::crelay::trans:bot ">nick" $chan $nick $newnick
	}
	
	# proc transmission of join
	proc trans:join {nick uhost hand chan} {
		if { $::crelay::debug == 1 } { dlog "Prepare transmission : >join $chan $chan $nick" }
		::crelay::trans:bot ">join" $chan $chan "$nick!$uhost"
	}
	
	# proc transmission of part
	proc trans:part {nick uhost hand chan text} {
		set arg [concat $chan $text]
		if { $::crelay::debug == 1 } { dlog "Prepare transmission : >part $chan $nick $arg" }
		::crelay::trans:bot ">part" $chan $nick $arg
	}
	
	# proc transmission of quit
	proc trans:quit {nick host hand chan text} {
		if { $::crelay::debug == 1 } { dlog "Prepare transmission : >quit $chan $nick $text" }
		::crelay::trans:bot ">quit" $chan $nick $text
	}
	
	# Proc to get our self quit
	proc trans:selfquit {type} {
		::crelay::trans:bot ">quit" $::crelay::me(chan) $::botnick [::msgcat::mc "I don't know why but I left server"]
	}
	
	# proc transmission of topic changement
	proc trans:topic {nick uhost hand chan topic} {
		if {$::crelay::topicsynced == 1 && [string tolower $nick] eq [string tolower $::botnick]} { 
         ::crelay::trans:bot ">act" $chan $nick "ACTION a mis à jour le topic sur $chan"
         return
      }
		set arg [concat $chan $topic]
		if { $::crelay::debug == 1 } { dlog "Prepare transmission : >topic $chan $nick $arg" }
		::crelay::trans:bot ">topic" $chan $nick $arg
	}
	
	# proc transmission of kick
	proc trans:kick {nick uhost hand chan victim reason} {
		set arg [concat $victim $chan $reason]
		if { $::crelay::debug == 1 } { dlog "Prepare transmission : >kick $chan $nick $arg" }
		::crelay::trans:bot ">kick" $chan $nick $arg
	}
	
	# proc transmission of mode changement
	proc trans:mode {from keyw text} {
		set nick [lindex [split $from !] 0]
		set chan [lindex [split $text] 0]
		set text [concat $nick $text]
		if { $::crelay::debug == 1 } { dlog "Prepare transmission : >mode $chan $nick $text" }
		::crelay::trans:bot ">mode" $chan $nick $text
	}
	
	# proc transmission of "who command"
	proc trans:who {nick uhost handle chan args} {
		if { [join [lindex [split $args] 0]] != "" } {
			set netindex [lsearch -nocase $::crelay::networks [lindex [split $args] 0]]
			if { $netindex == -1 } {
				putserv "PRIVMSG $nick :[::msgcat::mc "%1\$s is an unknown network" $args]";
				return 0
			} else {
			   set ::crelay::eol 0
			   set ::crelay::bol 0
					set ::crelay::eob 1
				putbot [lindex $::crelay::eggdrops $netindex] ">who $nick"
			}
		} else {
			set ::crelay::eol 0
			set ::crelay::bol 0
			::crelay::trans:bot ">who" $chan $nick ""
		}
	}
	
	# Error reception
	proc recv:error {frm_bot command arg} {
		# putlog "$command - $arg"
		return 0
	}
	
	# proc reception of pub
	proc recv:pub {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		if { $::crelay::debug == 1 } { dlog "Received $command from $frm_bot" }
		if {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
         set argl [split $arg]
         set unick [lindex $argl 0]
			set speaker [::crelay::make:user $unick $frm_bot]
			if { $::crelay::debug == 1 } { dlog "Sending pub [join [lrange $argl 1 end]] to $::crelay::me(chan)" }
			putchan $::crelay::me(chan) "$speaker [join [lrange $argl 1 end]]"
			::crelay::cr:log PUB $unick $speaker $::crelay::me(chan) [join [lrange $argl 1 end]]
		} else {
			if { $::crelay::debug == 1 } { dlog "$frm_bot is unknown" }
		}
		return 0
	}
	
	# proc reception of action
	proc recv:act {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		if { $::crelay::debug == 1 } { dlog "Received $command from $frm_bot" }
		if {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
			set argl [split $arg]
         set unick [lindex $argl 0]
			set speaker [::crelay::make:user $unick $frm_bot]
			if { $::crelay::debug == 1 } { dlog "Sending act [join [lrange $argl 2 end]] to $::crelay::me(chan)" }
			set text [::crelay::colorize "act" "* $speaker [join [lrange $argl 2 end]]"]
			putchan $::crelay::me(chan) $text
			::crelay::cr:log ACT $unick $speaker $::crelay::me(chan) [join [lrange $argl 2 end]]
		} else {
			if { $::crelay::debug == 1 } { dlog "$frm_bot is unknown" }
		}
		return 0
	}
	
	# proc reception of nick changement
	proc recv:nick {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		if { $::crelay::debug == 1 } { dlog "Received $command from $frm_bot" }
		if {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
			set argl [split $arg]
         set unick [lindex $argl 0]
			set speaker [::crelay::make:user $unick $frm_bot]
			if { $::crelay::debug == 1 } { dlog "Sending nick [join [lrange $argl 1 end]] to $::crelay::me(chan)" }
			set text [::crelay::colorize "jpq" [::msgcat::mc "*** %1\$s is now known as %2\$s" $speaker [join [lrange $argl 1 end]]]]
			putquick "PRIVMSG $::crelay::me(chan) :$text"
			::crelay::cr:log NICK $unick $speaker $::crelay::me(chan) [join [lrange $argl 1 end]]
		} else {
			if { $::crelay::debug == 1 } { dlog "$frm_bot is unknown" }
		}
		return 0
	}
	
	# proc reception of join
	proc recv:join {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		if { $::crelay::debug == 1 } { dlog "Received $command from $frm_bot" }
		if {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
			set argl [split $arg]
         set unick [lindex $argl 0]
			set speaker [lindex $argl 1]
			if { $::crelay::debug == 1 } { dlog "Sending join [join [lrange $argl 1 end]] to $::crelay::me(chan)" }
			set text [::crelay::colorize "jpq" [::msgcat::mc "--> %1\$s has joined %2\$s@%3\$s" $speaker $unick [lindex $::crelay::networks $him]]]
			putquick "PRIVMSG $::crelay::me(chan) :$text"
			::crelay::cr:log JOIN $unick $speaker $::crelay::me(chan) ""
		} else {
			if { $::crelay::debug == 1 } { dlog "$frm_bot is unknown" }
		}
		return 0
	}
	
	# proc reception of part
	proc recv:part {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		if { $::crelay::debug == 1 } { dlog "Received $command from $frm_bot" }
		if {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
			set argl [split $arg]
         set unick [lindex $argl 0]
			set speaker [::crelay::make:user $unick $frm_bot]
			if { $::crelay::debug == 1 } { dlog "Sending part [join [lrange $argl 1 end]] to $::crelay::me(chan)" }
			if {[llength $argl]<4} {
				set partmsg ""
			} else {
				set partmsg " ([join [lrange $argl 2 end]])"
			}
			set text [::crelay::colorize "jpq" [::msgcat::mc "<-- %1\$s has left %2\$s%3\$s" $speaker [lindex $argl 1] $partmsg]]
			putquick "PRIVMSG $::crelay::me(chan) :$text"
			::crelay::cr:log PART $unick $speaker $::crelay::me(chan) [join [lrange $argl 2 end]]
		} else {
			if { $::crelay::debug == 1 } { dlog "$frm_bot is unknown" }
		}
		return 0
	}
	
	# proc reception of quit
	proc recv:quit {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		if { $::crelay::debug == 1 } { dlog "Received $command from $frm_bot" }
		if {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
			set argl [split $arg]
         set unick [lindex $argl 0]
			set speaker [::crelay::make:user $unick $frm_bot]
			if {[llength $argl]<3} {
				set quitmsg ""
			} else {
				set quitmsg " ([join [lrange $argl 1 end]])"
			}
			if { $::crelay::debug == 1 } { dlog "Sending quit [join [lrange $argl 1 end]] to $::crelay::me(chan)" }
			set text [::crelay::colorize "jpq" [::msgcat::mc "-//- %1\$s has quit%2\$s" $speaker $quitmsg]]
			putquick "PRIVMSG $::crelay::me(chan) :$text"
			::crelay::cr:log QUIT $unick $speaker $::crelay::me(chan) [join [lrange $argl 1 end]]
		} else {
			if { $::crelay::debug == 1 } { dlog "$frm_bot is unknown" }
		}
		return 0
	}
	
	# proc reception of topic changement
	proc recv:topic {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		if { $::crelay::debug == 1 } { dlog "Received $command from $frm_bot" }
		if {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
			set argl [split $arg]
         set unick [lindex $argl 0]
			set speaker [::crelay::make:user $unick $frm_bot]
			if { $::crelay::debug == 1 } { dlog "Sending topic [join [lrange $argl 1 end]] to $::crelay::me(chan)" }
			if { $::crelay::me(syn_topic) == "y" } {
				set ::crelay::topicsynced 1
				utimer 10 [list set ::crelay::topicsynced 0]
				putserv "TOPIC $::crelay::me(chan) :[join [lrange $argl 2 end]]"
            ::crelay::cr:log TOPIC $unick $speaker $::crelay::me(chan) [join [lrange $argl 2 end]]
            putbot $frm_bot ">acktopic" $::crelay::me(chan)
			} else {
				putchan $::crelay::me(chan) [::msgcat::mc "*** %1\$s changes topic of %2\$s to '%3\$s'" $speaker [lindex $argl 1] [join [lrange $argl 2 end]]]
			}
		} else {
			if { $::crelay::debug == 1 } { dlog "$frm_bot is unknown" }
		}
		return
	}
   
   proc recv:acktopic {frm_bot $command $arg} {
      set frm_bot [string tolower $frm_bot]
		if { $::crelay::debug == 1 } { dlog "Received $command from $frm_bot" }
		if {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
         putchan $::crelay::me(chan) [::msgcat::mc "* Topic synced by %1\$s" [::crelay::make_user  $frm_bot $frm_bot]]
      } else {
			if { $::crelay::debug == 1 } { dlog "$frm_bot is unknown" }
		}
      return
   }
	
	# proc reception of kick
	proc recv:kick {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		if { $::crelay::debug == 1 } { dlog "Received $command from $frm_bot" }
		if {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
			set argl [split $arg]
			set speaker [::crelay::make:user [lindex $argl 1] $frm_bot]
			if { $::crelay::debug == 1 } { dlog "Sending kick [join [lrange $argl 1 end]] to $::crelay::me(chan)" }
			set text [::crelay::colorize "jpq" [::msgcat::mc "*** %1\$s has been kicked from %2\$s by %3\$s: %4\$s" $speaker [lindex $argl 2] [lindex $argl 0] [join [lrange $argl 3 end]]]]
			putquick "PRIVMSG $::crelay::me(chan) :$text"
			::crelay::cr:log KICK "$::crelay::me(chan)" "[lindex $argl 1] kicked from $::crelay::me(chan) by [lindex $argl 0]:[join [lrange $argl 3 end]]"
		} else {
			if { $::crelay::debug == 1 } { dlog "$frm_bot is unknown" }
		}
		return 0
	}
	
	# proc reception of mode changement
	proc recv:mode {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		if { $::crelay::debug == 1 } { dlog "Received $command from $frm_bot" }
		if {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
			set argl [split $arg]
			set speaker [::crelay::make:user [lindex $argl 1] $frm_bot]
			if { $::crelay::debug == 1 } { dlog "Sending mode [join [lrange $argl 1 end]] to $::crelay::me(chan)" }
			set text [::crelay::colorize "mode" [::msgcat::mc "*** %1\$s set mode %2\$s" $speaker [join [lrange $argl 2 end]]]]
			putquick "PRIVMSG $::crelay::me(chan) :$text"
		} else {
			if { $::crelay::debug == 1 } { dlog "$frm_bot is unknown" }
		}
		return 0
	}
	
	# reception of !who command
	proc recv:who {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		set nick $arg
		set op {}
      set hop {}
      set vop {}
      set uop {}
		if {![botonchan $::crelay::me(chan)]} {
			putbot $frm_bot ">wholist $::crelay::me(chan) $nick eol"
			return 0
		}
		foreach user [chanlist $::crelay::me(chan)] {
			if { $user == $::botnick } { continue; }
			if { [isop $user $::crelay::me(chan)] == 1 } {
				lappend op "@${user}"
			} elseif { [ishalfop $user $::crelay::me(chan)] == 1 } {
				lappend hop "%${user}"
			} elseif { [isvoice $user $::crelay::me(chan)] == 1 } {
				lappend vop "+${user}"
			} else {
				lappend uop ${user}
			}
		}
      set ulist [concat [lsort $op] [lsort $hop] [lsort $vop] [lsort $uop]]                  
		if { $ulist != "" } {
         foreach splul [::crelay::split_line $ulist [expr {$::crelay::msglen - [::tcl::string::length $::crelay::me(chan)]}]] {
            putbot $frm_bot ">wholist $::crelay::me(chan) $nick $splul"
         }
		}
		putbot $frm_bot ">wholist $::crelay::me(chan) $nick eol"
	}
	
	# Proc reception of a who list
	proc recv:wholist {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
      array set binfo $::crelay::regg($frm_bot)
		set nick [join [lindex [split $arg] 1]]
		set speaker [::crelay::make:user $binfo(display) $frm_bot]
		if {$::crelay::bol == 0} {
			incr ::crelay::bol
			putserv "NOTICE $nick :*** [::msgcat::mc "Beginning of userlist"]"
		}
		if { [join [lrange [split $arg] 2 end]] == "eol"} {
			incr ::crelay::eol
			if {$::crelay::eol == $::crelay::eob} {
				putserv "NOTICE $nick :*** [::msgcat::mc "End of userlist"]"
			}
		} else {
			putserv "NOTICE $nick :$speaker [join [lrange [split $arg] 2 end]]"
		}
	}
	
	# Special : botnet lost
	proc recv:disc {frm_bot} {
      set frm_bot [string tolower $frm_bot]
		if {$frm_bot == $::username} {
			putchan $::crelay::me(chan) [::msgcat::mc "I'd left the relay"]
		} elseif {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
			set speaker [::crelay::make:user "*" $frm_bot]
			putchan $::crelay::me(chan) [::msgcat::mc "*** We lose %1\$s (%2\$s leaves botnet)" $speaker $frm_bot]
		}
		return 0
	}
	
	# Special : botnet recover
	proc recv:link {frm_bot via} {
      set frm_bot [string tolower $frm_bot]
		if {$frm_bot == $::username} {
			putchan $::crelay::me(chan) [::msgcat::mc "I'm back in the relay"]
		} elseif {[set him [lsearch $::crelay::eggdrops $frm_bot]] >= 0} {
			set speaker [::crelay::make:user "*" $frm_bot]
			putchan $::crelay::me(chan) [::msgcat::mc "*** %1\$s is back (%2\$s rejoined botnet)" $speaker $frm_bot]
		}
		return 0
	}
	
	######################################
	# Private messaging
	#
	
	bind msg - "say" ::crelay::prv:say_send
	proc prv:say_send {nick uhost handle text} {
		set dest [join [lindex [split $text] 0]]
		set msg [join [lrange [split $text] 1 end]]
		set vict [join [lindex [split $dest @] 0]]
		set net [join [lindex [split $dest @] 1]]
		if { $vict == "" || $net == "" } {
			putserv "PRIVMSG $nick :[::msgcat::mc "Use \002say nick@network your message to \037nick\037\002"]"
			return 0
		}
		set him [lsearch -nocase $::crelay::networks $net]
		if { $him == -1 } {
			putserv "PRIVMSG $nick :[::msgcat::mc "%1\$s is an unknown network." $net]";
			putserv "PRIVMSG $nick :[::msgcat::mc "Available networks: %1\$s" [join [split $::crelay::networks]]]"
			return 0
		}
		if { [string length $msg] == 0 } {
			putserv "PRIVMSG $nick :[::msgcat::mc "Did you forget your message to %1\$s@%2\$s ?" $vict $net]"
			return 0
		}
		putbot [lindex $::crelay::eggdrops $him] ">pvmsg $vict $nick@$::crelay::me(network) $msg"
	}
	
	bind bot - ">pvmsg" ::crelay::prv:say_get
	proc prv:say_get {frm_bot command arg} {
      set frm_bot [string tolower $frm_bot]
		set dest [join [lindex [split $arg] 0]]
		set from [join [lindex [split $arg] 1]]
		set msg [join [lrange [split $arg] 2 end]]
		if { [onchan $dest $::crelay::me(chan)] == 1 } {
			putserv "PRIVMSG $dest :$from: $msg"
		}
	}

	proc putchan {chan msg} {
		if {[::tcl::string::match *c* [lindex [split [getchanmode $chan]] 0]]} {
			regsub -all "\017" [stripcodes abcgru $msg] "" msg
		}
		putquick "PRIVMSG $chan :$msg"
	}
	
	######################################
	# Small tools
	#
	proc checkDest { network } {
		if { $network eq "all" } { return 99 }
		set netindex [lsearch -nocase $::crelay::networks $network]
		if { $network ne "all" && $netindex == -1 } { return -1 }
		return $netindex
	}
	
	# Checks if eggdrop is @ or %
	proc hasRights { chan } {
		if { ![botisop $chan] && ![botishalfop $chan] } {
			return 0
		}
		return 1
	}
	
	# Split line function
	# based on MenzAgitat procedure
	# @see http://www.boulets.oqp.me/tcl/routines/tcl-toolbox-0014.html
	proc split_line {data limit} {
		incr limit -1
		if {$limit < 9} {
			error "limit must be higher than 9"
		}
		if { [::tcl::string::bytelength $data] <= $limit } {
			return [expr {$data eq "" ? [list ""] : [split $data "\n"]}]
		} else {
			set middle_pos [expr round($limit / 2.0)]
			set output ""
			while {1} {
				if { ([set cut_index [::tcl::string::first "\n" $data]] != -1) && ($cut_index <= $limit)} then {
				} elseif {
					([set cut_index [::tcl::string::last " " $data [expr {$limit + 1}]]] == -1)
					|| ($cut_index < $middle_pos)
				} then {
					set new_cut_index -1
					for {set i 0} {$i < 6} {incr i} {
						if {
							([::tcl::string::index $data [set test_cut_index [expr {$limit - $i}]]] eq "\003")
							&& ([regexp {^\003([0-9]{1,2}(,[0-9]{1,2})?)} [::tcl::string::range $data $test_cut_index end]])
						} then {
							set new_cut_index [expr {$test_cut_index - 1}]
						}
					}
					set cut_index [expr {($new_cut_index == -1) ? ($limit) : ($new_cut_index)}]
				}
				set new_part [::tcl::string::range $data 0 $cut_index]
				set data [::tcl::string::range $data $cut_index+1 end]
				if { [::tcl::string::trim [::tcl::string::map [list \002 {} \037 {} \026 {} \017 {}] [regsub -all {\003([0-9]{0,2}(,[0-9]{0,2})?)?} $new_part {}]]] ne "" } {
					lappend output [::tcl::string::trimright $new_part]
				} 
				if { [::tcl::string::trim [::tcl::string::map [list \002 {} \037 {} \026 {} \017 {}] [regsub -all {\003([0-9]{0,2}(,[0-9]{0,2})?)?} $data {}]]] eq "" } {
					break
				}
				set taglist [regexp -all -inline {\002|\003(?:[0-9]{0,2}(?:,[0-9]{0,2})?)?|\037|\026|\017} $new_part]
				set bold 0 ; set underline 0 ; set italic 0 ; set foreground_color "-1" ; set background_color "-1" 
				foreach tag $taglist {
					if {$tag eq ""} {
						continue
					}
					switch -- $tag {
						"\002" { if { !$bold } { set bold 1 } { set bold 0 } }
						"\037" { if { !$underline } { set underline 1 } { set underline 0 } }
						"\026" { if { !$italic } { set italic 1 } { set italic 0 } }
						"\017" { set bold 0 ; set underline 0 ; set italic 0 ; set foreground_color "-1" ; set background_color "-1" }
						default {
							lassign [split [regsub {\003([0-9]{0,2}(,[0-9]{0,2})?)?} $tag {\1}] ","] foreground_color background_color
							if {$foreground_color eq ""} {
								set foreground_color -1 ; set background_color -1
							} elseif {($foreground_color < 10) && ([::tcl::string::index $foreground_color 0] ne "0")} {
								set foreground_color 0$foreground_color
							}
							if {$background_color eq ""} {
								set background_color -1
							} elseif {
								($background_color < 10)
								&& ([::tcl::string::index $background_color 0] ne "0")
							} then {
								set background_color 0$background_color
							}
						}
					}
				}
				set line_start ""
				if {$bold} { append line_start \002 }
				if {$underline} { append line_start \037 }
				if {$italic} { append line_start \026 }
				if {($foreground_color != -1) && ($background_color == -1)} { append line_start \003$foreground_color }
				if {($foreground_color != -1) && ($background_color != -1)} { append line_start \003$foreground_color,$background_color }
				set data ${line_start}${data}
			}
			return $output
		}
	}
	
	######################################
	# proc for helping
	#
	# proc status
	proc help:status { nick host handle arg } {
		puthelp "PRIVMSG $nick :[::msgcat::mc "Chanrelay status for %1\$s@%2\$s" $::crelay::me(chan) $::crelay::me(network)]"
		puthelp "PRIVMSG $nick :[::msgcat::mc "\002 Global status\002"]"
		puthelp "PRIVMSG $nick :\037type\037   -- | trans -|- recept |"
		puthelp "PRIVMSG $nick :global -- | -- $::crelay::me(transmit) -- | -- $::crelay::me(receive) -- |"
		puthelp "PRIVMSG $nick :pub    -- | -- $::crelay::trans_pub -- | -- $::crelay::recv_pub -- |"
		puthelp "PRIVMSG $nick :act    -- | -- $::crelay::trans_act -- | -- $::crelay::recv_act -- |"
		puthelp "PRIVMSG $nick :nick   -- | -- $::crelay::trans_nick -- | -- $::crelay::recv_nick -- |"
		puthelp "PRIVMSG $nick :join   -- | -- $::crelay::trans_join -- | -- $::crelay::recv_join -- |"
		puthelp "PRIVMSG $nick :part   -- | -- $::crelay::trans_part -- | -- $::crelay::recv_part -- |"
		puthelp "PRIVMSG $nick :quit   -- | -- $::crelay::trans_quit -- | -- $::crelay::recv_quit -- |"
		puthelp "PRIVMSG $nick :topic  -- | -- $::crelay::trans_topic -- | -- $::crelay::recv_topic -- |"
		puthelp "PRIVMSG $nick :kick   -- | -- $::crelay::trans_kick -- | -- $::crelay::recv_kick -- |"
		puthelp "PRIVMSG $nick :mode   -- | -- $::crelay::trans_mode -- | -- $::crelay::recv_mode -- |"
		puthelp "PRIVMSG $nick :who    -- | -- $::crelay::trans_who -- | -- $::crelay::recv_who -- |"
		if { $::crelay::me(syn_topic) == "y"} {
			puthelp "PRIVMSG $nick :[::msgcat::mc "Transmission of %1\$s enabled" "Topic"]"
		} else {
			puthelp "PRIVMSG $nick :[::msgcat::mc "Transmission of %1\$s disabled" "Topic"]"
		}
		puthelp "PRIVMSG $nick :[::msgcat::mc "Nicks appears as %1\$s%2\$s%1\$s" $::crelay::hlnick $nick]"
		puthelp "PRIVMSG $nick :--- *** ---"
	}
		
	# proc help
	proc help:cmds { nick host handle arg } {
		puthelp "NOTICE $nick :[::msgcat::mc "/msg %1\$s trans <type> on|off to change the transmissions" $::botnick]"
		puthelp "NOTICE $nick :[::msgcat::mc "/msg %1\$s recv <type> on|off to change the receptions" $::botnick]"
		puthelp "NOTICE $nick :[::msgcat::mc "/msg %1\$s rc.status to see my actual status" $::botnick]"
		puthelp "NOTICE $nick :[::msgcat::mc "/msg %1\$s rc.help for this help" $::botnick]"
		puthelp "NOTICE $nick :[::msgcat::mc "/msg %1\$s rc.light <bo|un|gr|off> to bold, underline, gray or no higlight" $::botnick]"
		puthelp "NOTICE $nick :[::msgcat::mc "/msg %1\$s rc.net <yes|no> to (un)show the network" $::botnick]"
		puthelp "NOTICE $nick :[::msgcat::mc "/msg %1\$s rc.syntopic <yes|no> to disable/enable the topic synchronisation" $::botnick]"
		puthelp "NOTICE $nick :[::msgcat::mc "/msg %1\$s rc.debug \[<yes|no>\] to disable/enable the debug (no arg: display debug status)" $::botnick]"
	}
	
	# Debug log
	proc dlog {text} {
		set out [open ${::crelay::crpath}${::crelay::crlog} a]
		puts $out "\[[clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"]\] <$::username> $text"
		close $out
	}
}

proc i18n {msgfile {lang {}} } {
	if {$lang == ""} {set lang [string range [::msgcat::mclocale] 0 1] }
	if { [catch {open $msgfile r} fmsg] } {
		putlog "\002ChanRelay\002 - Could not open $msgfile for reading\n$fmsg"
	} else {
		putlog "\002ChanRelay\002 - Loading $lang file from $msgfile"
		while {[gets $fmsg line] >= 0} {
			lappend ll $line
		}
		close $fmsg
		::msgcat::mcmset $lang [join $ll]
		unset ll
	}
	::msgcat::mclocale $lang
}

if {![catch {package require msgcat}] && $::crelay::lang != "" && [string tolower $::crelay::lang] != "english"} {
	set lfile "${::crelay::crpath}chanrelay.[string tolower $::crelay::lang].msg"
	if {[file exists $lfile]} {
		i18n $lfile [::tcl::string::map {"english" "en" "french" "fr" "spanish" "sp" "german" "de" "portuguese" "pt"} [string tolower $::crelay::lang]]
	}
} else {
	# short hack if msgcat cannot be load
	namespace eval ::msgcat {
		proc mc {text {str ""} args} { return [format $text $str $args] }
	}
}

::crelay::init

putlog "CHANRELAY $::crelay::version by \002$::crelay::author\002 loaded - https://www.eggdrop.fr"
