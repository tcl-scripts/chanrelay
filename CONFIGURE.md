# Configuration of chanrelay

The configuration of ChanRelay is quite simple but a small guide can be usefull

Remember that if you have several eggdrop on the same hosting and in the same relay, they can (must ?) share the same tcl, no need to have a copy of the script per eggdrop.

**Also remember that the name in the `regg()` array is case-sensitive and must be the username of the eggdrop.**
## Basics
**For each eggdrop in the relay, you must add an entry in the `regg(<eggdrop>)` array. Note that the `<eggdrop>` value must be the username of the eggdrop and others eggdrop must know it with this name.**

It means that if your eggdrop has _Lambda_ as **username** (setted in eggdrop.conf), you must have added it in other eggdrops with this botname (`.+bot Lambda the.host 5555`) and your regg entry is `set regg(Lambda) { ... }`

## Globals settings
| param | type | def. val | explanation |
| ------ | ------ | ------ | ------ |
| *debug* | int | 0 | disable or enable the debug. Can be overrided with `/msg <eggdrop> rc.debug on/off` |
| *lang* | string | english | change the language of output. Actually, only english and french |
| *users_excluded* | list | | list of users which won't be relayed. Think to escape chars as [, ], { and } |
| *users_only* | list | | if this list is filled, ChanRelay will relay **only these users** |
| *bantype* | int | 1 | mask to use for bans |
| *msglen* | int | 350 | the max length of a relayed message. If the message is longer, it will be split in several lines |


### Default transmission and reception
You can set the default mode for transmissions and receptions by setting (y)es or (n)o.

Each setting can be overrided using the `/msg <eggdrop> trans <type> <on|off>` and `/msg <eggdrop> recv <type> <on|off>` where *type* is one of *pub* (public message), *act* (action, or /me), *nick* (nick change), *join*, *part, *quit*, *topic* (for topic changement), *kick*, *mode* and *who* (command to list users in the relay)

- *ban* is not a setting as it is a mode changement
- if the *trans who* setting is set to off, it means the eggdrop won't react to the !who command
- if the *recv who* setting is set to off, it means the eggdrop won't receive who' answers from other bots and won't sent its who list

### Files
Note that all files (exeption for virtual chan logfile) will be automatically generated in `chanrelay_place/crtools/`, where *chanrelay_place* is the place of *chanrelay.tcl*. As the directory is in the package, it exists :)
1. **crconf** : name of the eggdrop config file. The %b string will be replaced with botname. example: `crconf "%b.chanrelay.db"` will create a file named *lamer.chanrelay.db* if your eggdrop is named *lamer*
2. **crlog** : name of the debug file

## Eggdrop specific settings
### Defaults values
This settings are in `default` and can be overrided for each eggdrop

| param | type | default | description |
| ------ | ------ | ------ | ------ |
| highlight | int | 1 | the type of highlight : 0 => off, 1 => bold, 2 => underline, 3 => gray |
| snet | char | y | show or not the network in the fake user name |
| transmit | char | y | does the eggdrop transmit the channel activity to relay ? |
| receive | char | y | does the eggdrop receive relay activity ? |
| log | char | y | does the eggdrop log relay activity in its channel log ? |
| syn_topic | char | n | does eggdrop synchronize topic when it's changed somewhere in the relay ? |
| col_act | string | purple | color of the line for an action (/me) |
| col_jpq | string | cyan | color of the line for a join/part/quit |
| col_mode | string | green | color of the line for a mode changement |
| user_mask | string | (%nick%@%network%) | mask used to display the user. %nick% will be replaced with the user nick and %network% with the value setted in regg(eggdrop)(network) |

### Eggdrop specific settings
This settings are mandatory:
- **channel** : the name of the relay channel for this eggdrop
- **network** : a friendly name for the network where is the eggdrop

All others settings in `default` can be added in specific eggdrop configuration to override.

### Notes about settings
- if **transmit** or **receive** is set to n, the eggdrop won't transmit/receive anything
- colors are one of the following: white, black, blue, green, lightred, brown, purple, orange, yellow, lightgreen, cyan, lightcyan, lightblue, pink, grey, lightgrey
