source scripts/picoirc060.tcl

namespace eval ::cloner {

   package require hook
   package require picoirc 0.6.0

	variable net "irc://irc.zeolia.net/"

   hook bind cloner join clone ::cloner::hrun 
   proc hrun {bnick chan} {
      ::cloner::startme $bnick ${::cloner::net}${chan}
   }
   
   hook bind cloner quit clone ::cloner::hkill
   proc hkill {bnick reason} {
      ::picoirc::post $::cloner::clones($bnick) "" "/quit $reason"
      unset ::cloner::clones($bnick)
   }
   
   hook bind cloner pub clone ::cloner::hpub
   proc hpub {bnick dest msg} {
      ::picoirc::post $::cloner::clones($bnick) $dest "/msg $msg"
   }
   
   hook bind cloner act clone ::cloner::hact
   proc hact {bnick dest msg} {
		::picoirc::post $::cloner::clones($bnick) $dest "/me $msg"
   }
   
   hook bin cloner nick clone ::cloner::hnick
   proc hnick {bnick newnick} {
		::picoirc::post $::cloner::clones($bnick) "" "/nick $newnick"
   }
   
   variable myirc
   variable clones
   
   proc startme {nick {uri ""}} {
      ::picoirc::initver "Relay cloner 0.1a"
	  if {$uri eq ""} { set uri "irc://irc.zeolia.net/#raspfr" }
      set ::cloner::myirc($nick) [::picoirc::connect ::cloner::cbirc $nick $uri "" $nick]
   }

   proc cbirc {context state args} {
      switch $state {
         init {
            puts "Initialized with $context"
         }
         connect {
            puts "Connection with $context"
         }
         close {
            puts "Well, close with $args"
         }
         nickchange {
            lassign [split $args] oldnick newnick
            putlog "$oldnick to $newnick"
            if {[info exists ::cloner::myirc($oldnick)]} {
               set ::cloner::myirc($newnick) $::cloner::myirc($oldnick)
               unset ::cloner::myirc($oldnick)
            }
         }
         debug {
            set datas [split [lindex $args 1]]
            if {[lindex $datas 0] eq "NICK"} {
               set ::cloner::clones([lindex $datas 1]) $context
            }
            puts "D --> $args"
         }
         system {
            set datas [split [lindex $args 1]]
            puts "System sent $args"
         }
         traffic {
            # puts "TRAFFIC : $args"
         }
         version {
            puts $context "ChanRelay:5.0.0:Cloner 1.0"
            return 0
         }
         chat {
            putlog "CHAT : $args"
            set pm 0
            lassign $args target who msg type
            if {[info exists ::cloner::myirc($target)]} {
               set pm 1
               # PRIVMSG I guess
               putlog "$who told me $msg"
            }
            #if {[lindex $args 0] ne "testor" } {
            #   return
            #}
            switch [lindex $msg 0] {
               chut {
                  if {$pm == 1} {
                     ::picoirc::post $context "" "/quit"
                     unset context
                     set ::cloner::closeme($target) 1
                  }
               }
            }
           }
         default {
            puts $args
            }
        }
   }
}