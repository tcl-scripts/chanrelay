# source scripts
# .tcl source scripts/crtools/cloner.tcl
#.tcl ::cloner::cjoin toto ici@pas.la * #adultes

source [file dirname [file normalize [info script]]]//picoirc060.tcl
namespace eval ::cloner {

    variable dest
    variable src
    set dest(network) {"irc://bds.zeolia.chat" "irc://avalon.zeolia.net"}
    set dest(chan) "#tchat"
    set src(chan) "#insomniaque"

    variable myirc
   package require picoirc 0.6.0

   bind part - "$::cloner::src(chan) *" ::cloner::cpart
   bind sign - "$::cloner::src(chan) *" ::cloner::cpart
   bind kick - "$::cloner::src(chan) *" ::cloner::ckick
   
   proc ckick {nick uhost handle channel target reason} {
    if {![info exists ::cloner::myirc($target)]} { return }
    ::picoirc::post $::cloner::myirc($target) "" "/quit kick : $reason"
   }
   
   proc cpart {nick uhost handle chan text} {
    if {![info exists ::cloner::myirc($nick)]} { return }
    ::picoirc::post $::cloner::myirc($nick) "" "/quit $nick"
    unset ::cloner::myirc($nick)
   }
   
   
   bind pubm - "$::cloner::src(chan) *" ::cloner::cpubm
   proc cpubm {nick uhost handle chan text} {
	if {[string match "*chaat*" $text]} { putlog "I get $text"; return }
    if {[string index $text 0] eq "\001"} { return }
    if {![info exists ::cloner::myirc($nick)]} {
        ::cloner::cjoin $nick $uhost $handle $chan
    }
      ::picoirc::post $::cloner::myirc($nick) $::cloner::dest(chan) "/msg $::cloner::dest(chan) $text"
   }
   
   bind ctcp - "ACTION" ::cloner::cact
   proc cact {nick uhost handle chan key text} {
	if {[string match "*chaat*" $text]} { return }
    if {![info exists ::cloner::myirc($nick)]} {
        ::cloner::cjoin $nick $uhost $handle $chan
    }
		::picoirc::post $::cloner::myirc($nick) $::cloner::dest(chan) "/me $text"
   }
   
   bind nick - "$::cloner::src(chan) *" ::cloner::cnick
   proc cnick {nick uhost handle chan newnick} {
        if {![info exists ::cloner::myirc($nick)]} {
            ::cloner::cjoin $newnick $uhost $handle $chan
        } else {
            ::picoirc::post $::cloner::myirc($nick) "" "/nick $newnick"
        }
   }
   
   proc fakeasv {nick} {
    # return "[expr {13+round(rand()*50)}] / [lindex {H F T} [rand 3]] / Ailleurs"
    return $nick
   }
   
   
   if {[string match -nocase *extended-join* [cap enabled]] && [string match -nocase *extended-join* [cap ls]]} {
      cap req extended-join
      bind RAWT - JOIN ::cloner::ejoin
   } else {
      bind join - "$::cloner::src(chan) *" ::cloner::cjoin
   }
   
   proc ejoin {from kw text flag} {
    putlog $from
        regexp -- {^([^!]+)!([^@]+)@(.+)} $from - nick username host
      regexp -- {([^\s]+)\s([^\s]+)\s:(.+)} $text - chan account realname
      if {[isbotnick $nick]} { return }
      ::picoirc::initver $realname
      set tserv [lindex $::cloner::dest(network) [rand [llength $::cloner::dest(network)]]]
      set uri "${tserv}/${::cloner::dest(chan)}"
      putlog "Connecting $nick to $uri"
      set ::cloner::myirc($nick) [::picoirc::connect ::cloner::cbirc $nick $uri "" $username]
   }
   
   # 
   proc cjoin {nick uhost handle chan} {
    if {[isbotnick $nick]} { return }
        putlog "Must start for $nick"
        ::picoirc::initver [::cloner::fakeasv $nick]
        set tserv [lindex $::cloner::dest(network) [rand [llength $::cloner::dest(network)]]]
        set uri "${tserv}/${::cloner::dest(chan)}"
        putlog "Connecting to $uri"
        set ::cloner::myirc($nick) [::picoirc::connect ::cloner::cbirc $nick $uri "" $nick]
        # putlog "::picoirc::connect ::cloner::cbirc $nick $uri"
    }

   proc cbirc {context state args} {
      switch $state {
         init {
            puts "Initialized with $context"
         }
         connect {
            puts "Connection with $context"
         }
         close {
            puts "Well, close with $args"
         }
         nickchange {
            lassign [split $args] oldnick newnick
            # putlog "$oldnick to $newnick"
            if {[info exists ::cloner::myirc($oldnick)]} {
               set ::cloner::myirc($newnick) $::cloner::myirc($oldnick)
               unset ::cloner::myirc($oldnick)
            }
         }
         debug {
            set datas [split [lindex $args 1]]
            if {[lindex $datas 0] eq "NICK"} {
               set ::cloner::myirc([lindex $datas 1]) $context
            }
            puts "D --> $args"
         }
         system {
            set datas [split [lindex $args 1]]
            puts "System sent $args"
         }
         traffic {
            # puts "TRAFFIC : $args"
         }
         version {
            puts $context "WeeChat 3.8"
            return 0
         }
         chat {
            # putlog "CHAT : $args"
            set pm 0
            lassign $args target who msg type
            if {[info exists ::cloner::myirc($target)]} {
               set pm 1
               # ::picoirc::post $context "" "/msg $who Désolée mais je ne parle que sur libre-tchat.net"
               # PRIVMSG I guess
               putlog "$who told me $msg"
            }
            #if {[lindex $args 0] ne "testor" } {
            #   return
            #}
            switch [lindex $msg 0] {
               chut {
                  if {$pm == 1} {
                     ::picoirc::post $context "" "/quit"
                     unset context
                     set ::cloner::closeme($target) 1
                  }
               }
            }
           }
         default {
            puts $args
            }
        }
   }
   
   proc deinit {} {
    namespace delete [namespace current]
   }
   bind evnt - prerehash [namespace current]::deinit
}
