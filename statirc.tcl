# Stats IRC logger
# Logs pub and actions
# quit / kick / part / join
# topic and nick changes

# to enable on #channel:
# .chanset #channel +dblog
# to enable self logging (log of bot pub and action):
# .chanset #channel +logme

namespace eval statirc {

   variable db
   # Database settings
   set db {
      "host" "127.0.0.1"
      "port" "3306"
      "base" "eggdrop"
      "user" "eggdrop"
      "pass" "*********"
      "table" "irc_stats"
   }

   # Usermasks to ignore (for later use)
   variable ignore {"services.zeolia.net"}

   # Don't edit
   variable dbconn
   variable cnx

   package require mysqltcl
   setudef flag dblog
   setudef flag logme

   variable sauthor "CrazyCat <https://forum.eggdrop.fr>"
   variable sname "Stats IRC Logger"
   variable sversion "1.0"

   proc init {} {
      if {![string match *account-notify* [cap enabled]] && [string match -nocase *account-notify* [cap ls]]} {
         cap req account-notify
      }
      array set ::statirc::dbconn $::statirc::db
      ::statirc::db.create
      putlog "$::statirc::sname V$::statirc::sversion by $::statirc::sauthor loaded"
   }

   # Normal proc
   proc logpubm {nick uhost handle chan text} {
      if {[string index $text 0] eq "\001"} { return }
      if {[::statirc::canlog $chan $nick]==1} {
         ::statirc::send2db $nick $chan "PUB" $text
      }
   }
   proc logact {nick uhost handle chan key text} {
      if {[::statirc::canlog $chan $nick]==1} {
         ::statirc::send2db $nick $chan "ACT" $text
      }
   }
   proc lognick {nick uhost handle chan newnick} {
      if {[::statirc::canlog $chan $nick]==1} {
         ::statirc::send2db $nick $chan "NICK" "" $newnick
      }
   }
   proc logjoin {nick uhost handle chan} {
      if {[::statirc::canlog $chan $nick]==1} {
         ::statirc::send2db $nick $chan "JOIN" ""
      }
   }
   proc logpart {nick uhost handle chan text} {
      if {[::statirc::canlog $chan $nick]==1} {
         ::statirc::send2db $nick $chan "PART" $text
      }
   }
   proc logsign {nick uhost handle chan text} {
      if {[::statirc::canlog $chan $nick]==1} {
         ::statirc::send2db $nick $chan "QUIT" $text
      }
   }
   proc logkick {nick uhost handle chan target text} {
      if {[::statirc::canlog $chan $nick]==1} {
         ::statirc::send2db $nick $chan "KICK" $text $target
      }
   }
   proc logtopic {nick uhost handle chan text} {
      if {[::statirc::canlog $chan $nick]==1} {
         ::statirc::send2db $nick $chan "TOPIC" $text
      }
   }

   proc logme {queue message status} {
      set msg [list {*}$message]
      set msg [string range [join [lassign $msg mtype chan]] 1 end]
      if {[validchan $chan] && [channel get $chan logme]} {
         switch $mtype {
            MODE {
               # to do
            }
            PRIVMSG {
               if {[string first "\001ACTION" $msg] == 0} {
                  set msg [string range $msg 8 end]
                  ::statirc::logact $::botnick * * $chan "ACTION" $msg
               } else {
                  ::statirc::logpubm $::botnick * * $chan $msg
               }
            }
         }
      }
   }

   # Hook proc
   proc hpubm {nick chan text} { ::statirc::logpubm $nick * * $chan $text }
   proc hact {nick chan text} { ::statirc::logact $nick * * $chan "ACTION" $text }
   proc hnick {nick chan newnick} { ::statirc::lognick $nick * * $chan $newnick }
   proc hjoin {nick chan} { ::statirc::logjoin $nick * * $chan }
   proc hpart {nick chan text} { ::statirc::logpart $nick * * $chan $text }
   proc hsign {nick chan text} { ::statirc::logsign $nick * * $chan $text }
   proc hkick {nick chan target text} { ::statirc::logkick $nick * * $chan $target $text }
   proc htopic {nick chan text} { ::statirc::logtopic $nick * * $chan $topic }

   # Utilities
   # checks if data must be logged
   proc canlog {chan nick} {
      if {[validchan $chan] && [channel get $chan dblog] && (![isbotnick $nick] || ([isbotnick $nick] && [channel get $chan logme])) && $nick ne "*"} {
         return 1
      } else {
         return 0
      }
   }

   # returns account, or nick if account is unavailable
   proc nickoraccount {nick} {
      if {![string match *account-notify* [cap enabled]]} { return $nick }
      set acc [getaccount $nick]
      if { $acc eq ""} { return $nick }
      return $acc
   }

   # Database tools
   proc db.open {} {
      set ::statirc::cnx [::mysql::connect -host $::statirc::dbconn(host) -port $::statirc::dbconn(port) -user $::statirc::dbconn(user) -password $::statirc::dbconn(pass) -encoding binary]
      if {[::mysql::state $::statirc::cnx] < 3} {
         ::statirc::db.close
      } else {
         ::mysql::use $::statirc::cnx $::statirc::dbconn(base)
      }
   }

   proc db.close {} {
      ::mysql::close $::statirc::cnx
      unset -nocomplain ::statirc::cnx
   }

   proc db.create {} {
      ::statirc::db.open
      set tables [split [::mysql::info $::statirc::cnx tables]]
      if {[lsearch -exact $tables $::statirc::dbconn(table)] == -1} {
         set qcreate "CREATE TABLE $::statirc::dbconn(table) ( \
channel varchar(250) NOT NULL, \
eggdrop varchar(32) NOT NULL, \
ts datetime NOT NULL, \
type varchar(20) NOT NULL, \
nick varchar(32) NOT NULL, \
rawc text NOT NULL DEFAULT '', \
content text NOT NULL DEFAULT '', \
len int(20) NOT NULL, \
target varchar(32) NOT NULL \
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;"
         ::mysql::exec $::statirc::cnx $qcreate
         set qindex "ALTER TABLE $::statirc::dbconn(table) \
ADD KEY channel (channel), \
ADD KEY nick (nick);"
         ::mysql::exec $::statirc::cnx $qindex
      }
      ::statirc::db.close
   }

   proc send2db { nick chan type text {target ""}} {
      set ts [clock format [clock seconds] -format "%G-%m-%d %T"]
      set dnick [::statirc::nickoraccount $nick]
      set stripped [stripcodes bcruag $text]
      regsub -all -- {[^\w\s]} $stripped {} stripped
      set len [llength [split [string trim $stripped]]]
      ::statirc::db.open
      ::mysql::exec $::statirc::cnx "INSERT INTO $::statirc::dbconn(table) (channel, eggdrop, ts, type, nick, rawc, content, len, target) VALUES ('$chan', '$::botnick', '$ts', '$type', '[::mysql::escape $::statirc::cnx $dnick]', '[::mysql::escape $::statirc::cnx $text]', '[::mysql::escape $::statirc::cnx $stripped]', $len, '[::mysql::escape $::statirc::cnx $target]')"
      ::statirc::db.close
   }

   # Normal binds
   bind pubm - * ::statirc::logpubm
   bind ctcp - "ACTION" ::statirc::logact
   bind nick - * ::statirc::lognick
   bind join - * ::statirc::logjoin
   bind part - * ::statirc::logpart
   bind sign - * ::statirc::logsign
   bind kick - * ::statirc::logkick
   bind topc - * ::statirc::logtopic
   bind out - "% sent" ::statirc::logme

   if {![catch {package require hook}]} {
      # Hooks
      hook bind logger pubm statirc ::statirc::hpubm
      hook bind logger action statirc ::statirc::hact
      hook bind logger nick statirc ::statirc::hnick
      hook bind logger join statirc ::statirc::hjoin
      hook bind logger part statirc ::statirc::hpart
      hook bind logger sign statirc ::statirc::hsign
      hook bind logger kick statirc ::statirc::hkick
      hook bind logger topic statirc ::statirc::htopic
   }

   ::statirc::init

}